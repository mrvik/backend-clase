class Dispatcher{
    constructor(){
        this.events=[];
    }

    on(eventName, callback){
        if(this.events[eventName])this.events[eventName].push(callback);
        else this.events[eventName]=[callback];
    }

    fire(eventName,...args){
        console.info(`Dispatching event ${eventName}: `,args);
        if(this.events[eventName])this.events[eventName].forEach(event=>event(...args))
    }

}

module.exports=Dispatcher;
