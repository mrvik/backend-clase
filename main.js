#!/usr/bin/env node

const {IO}=require("./lib/io");
const {DB}=require("./lib/db");

async function main(){
    const port=process.env.PORT;
    var conn=new IO(port);
    var db=new DB();
    var controller=new Controller(conn,db);
}

class Controller{
    constructor(socket,database){
        this.socket=socket;
        this.database=database;
        this.createEvents();
    }

    createEvents(){
        this.socket.on("change", event=>{
            this.database.processEvent("change", event);
        });
        this.database.on("dbchange", event=>{
            this.socket.processEvent("dbchange", event);
        })
    }
}

main();
