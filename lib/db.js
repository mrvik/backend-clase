const mysql=require("mysql");
const Dispatcher=require("./dispatcher");
const sqlEscape=require("sqlstring").escape;

class DB extends Dispatcher{
    constructor(options){
        super();
        let opt=options||this.auth();
        this.lock=this.init(opt);
    }

    async init(options){
        this.pool=mysql.createPool(options);
        return await new Promise((r,j)=>{
            this.pool.query("SELECT 1+1 AS solucion",err=>{
                if(err)return j(err);
                r(true);
            });
        })
            .then(res=>{
                if(res===true)console.info("DB connected: "+res);
            })
    }

    async query(query){
        await this.lock;
        let maxTries=4;
        var err;
        for(let tries=0; tries<maxTries; tries++){
            try{
                let conn=await new Promise((resolver,rechazar)=>{
                    this.pool.getConnection((e,c)=>{
                        if(e)return rechazar(e);
                        return resolver(c);
                    });
                });
                let X=await new Promise((resolver,rechazar)=>{ //eslint-disable-line no-loop-func
                    conn.query(query, (er, res, campos)=>{
                        if(err) return rechazar(er);
                        resolver({res, campos});
                    });
                }).finally(()=>{
                    conn.release();
                })
                return X;
            }catch(e){
                err=e;
                continue;
            }
        }
        console.error("Max tries exceeded while connecting to MySQL");
        throw err;

    }

    auth(){
        let host=process.env.MYSQL_HOST;
        let user=process.env.MYSQL_USER;
        let password=process.env.MYSQL_PASSWORD;
        let database=process.env.MYSQL_DATABASE;
        return {host, user, password, database};
    }

    processEvent(event, ...args){
        switch(event){
            case "change": {
                let evt=args[0];
                console.log(event,args);
                return this.updateDB(evt);
            }
            default:
                throw new Error("Unexpected event "+event);
        }
    }

    async updateDB(data){
        let {asignatura,cara,comentario}=this.escapeObj(data);
        let q=`INSERT INTO ${this.table} (asignatura, cara, comentario) VALUES (${asignatura}, ${cara}, ${comentario})`;
        let q2=`SELECT FROM ${this.table} (asignatura, cara, comentario)`;
        return console.info(q);
        await this.query(q);
        let result=await this.query(q2).then(this.parseResult);
        this.fire("dbchange", result);
    }

    /**
     * Entrada esperada: {asignatura, cara, comentario}
     * @param {Object} dataObject Objeto con datos
     * @param {String} dataObject.asignatura String de datos (dentro de las asignaturas posibles)
     * @param {Number} dataObject.cara Número de cara
     * @param {String} dataObject.comentario Comentario sobre la asignatura
     * @return {Object} Igual que la entrada con los valores para SQL
     */
    escapeObj(dataObject){
        let rt={};
        for(let prop in dataObject){
            if(!dataObject.hasOwnProperty(prop))continue;
            rt[prop]=sqlEscape(dataObject[prop]);
        }
        return rt;
    }

    parseResult(dataFromDBZ){
        //Hacer cosas aquí
    }
}

module.exports={DB}
