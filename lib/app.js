const express=require("express");
const bodyParse=require("body-parser");
const Dispatcher=require("./dispatcher;")
const app=express();
const port=process.env.PORT||8080;

class App extends Dispatcher{
    constructor(){
        super();
        this.configure();
    }

    configure(){
        app.use(bodyParse.json());
        app.use(bodyParse.urlencoded({extended: true}))
        app.get("/",res=>{
            res.status(404).send("No way!");
        });
        app.post("/",this.handlePost);
    }

    start(){
        app.listen(port);
    }

    handlePost(req,res){
        let {body}=req;
        console.info(body);
    }

    get server(){
        return app;
    }
}

module.exports=App;
