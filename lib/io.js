const Server=require("socket.io");
const Dispatcher=require("./dispatcher");
const http=require("http");

const defaultP=`
<!DOCTYPE html>
<html>
    <head>
        <title>Esto es algo</title>
        <meta charset="UTF-8"/>
        <script src="/socket.io/socket.io.js"></script>
        <script>self.p=new io();</script>
    </head>
    <body>
        <h1>Nada</h1>
    </body>
</html>
`;

class IO extends Dispatcher{
    constructor(port=8080,httpServer,opt){
        super();
        let options=opt||{
            serveClient: true
        }
        this._server=httpServer||this.createServer(port);
        this.io=new Server(this._server,options);
        this.createEvents();
    }

    createServer(port){
        console.info("Creating server");
        let server=http.createServer(this.handler);
        server.listen(port);
        return server;
    }

    createEvents(){
        this.io.on("connection", socket=>{
            console.info("User connected");
            socket.on("change", (...args)=>this.fire("change",...args));
        });
    }

    get server(){
        return this._server;
    }

    handler(req,res){
        if(!req.url.match("socket.io")){
            res.writeHead(200, {contentType: "text/html"});
            res.write(defaultP);
            return res.end();
        }
    }

    processEvent(name, ...args){
        switch(name){
            case "dbchange": {
                return this.io.emit("dbchange", ...args);
            }
            default:
                throw new Error(`Unhandled event ${name}`);
        }
    }
}

module.exports={IO};

